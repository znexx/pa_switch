# pa\_switch

A quick and dirty Bash script to move the audio output of all running programs to the next sound card you have installed, or move the inputs to all running programs to the next input device.

## Features

* Simple, does what it should do. No more, no less
* Lightweight, the executable ticks in just above 2 kB

## Dependencies

* [pactl](https://linux.die.net/man/1/pactl) - A part of the pulseaudio-utils package in Debian and Ubuntu, and `libpulse` in Arch Linux. Part of the [PulseAudio](https://www.freedesktop.org/wiki/Software/PulseAudio/) project.
* [notify-send](https://ss64.com/bash/notify-send.html) - A program from GNOME's [libnotify](https://gitlab.gnome.org/GNOME/libnotify) to send notifications to your notification daemon. Is optional.

## Installation
1. Make sure the script is executable by running `chmod +x pa_switch` on it.
2. Put `pa_switch` in your `/bin/`, `/usr/bin/`, `~/bin/`, or any other directory in your PATH environment variable.

## Example usage - Openbox

I have this keyboard bind in Openbox to quickly switch between my headphones and loudspeakers with `Ctrl+Alt+s`, and between my headset mic and laptop mic with `Ctrl+Alt+Shift+s`.

```
    <keybind key="C-A-s">
      <action name="Execute">
        <startupnotify>
          <enabled>true</enabled>
          <name>Switch auto output</name>
        </startupnotify>
        <command>pa_switch</command>
      </action>
    </keybind>
    <keybind key="C-A-S-s">
      <action name="Execute">
        <startupnotify>
          <enabled>true</enabled>
          <name>Switch auto input</name>
        </startupnotify>
        <command>pa_switch -s</command>
      </action>
    </keybind>
```

## Usage
If you have followed the installation instructions you can just run:
```
$ pa_switch
```

If you don't have it installed, you can of course still run it as-is with one of these commands:
```
$ ./pa_switch
$ bash pa_switch
```

You might have to make it runnable by first performing `chmod +x pa_switch`.

## Copyright

This application is released under the `WTFPL`:

```text
DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
Version 2, December 2004

Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.

DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

0. You just DO WHAT THE FUCK YOU WANT TO.
```

 All used resources are copyright of their respective owners.
